**ironFetus**

A cross-platform game library that lets you do what you want.

*Note: This library is in alpha, and I have no intention of supporting other users at the moment (or ever). It is very likely to have breaking changes. Use at your own risk.*
