package ironFetus.text;

import ironFetus.Graphic;
import ironFetus.Math;
import ironFetus.Vector2;
import ironFetus.text.BitmapFont;
import ironFetus.text.Glyph;
import ironFetus.text.TextAlign;
import ironFetus.text.TextAnchor;


class Text
{
    public var pos:Vector2;
    public var scale:Vector2;
    public var angle:Float;
    public var color:Int;
    public var depth:Int;

    public var font:BitmapFont;
    public var length:Int;
    public var glyphGraphics:Array<Graphic>;
    public var text:String;
    public var anchor:TextAnchor;
    public var align:TextAlign;

    private var cursor:Int;
    private var cursorX:Float;
    private var cursorY:Float;
    private var glyph:Glyph;
    private var glyphX:Float;
    private var glyphY:Float;
    private var textWidth:Float;
    private var textLineWidths:Array<Float>;
    private var textHeight:Float;
    private var lineIndex:Int;
    private var lineGlyphs:Array<Array<Graphic>>;

    public function new(l:Int, f:BitmapFont)
    {
        length = l;
        font = f;

        glyphGraphics = [];
        for (i in 0...length)
        {
            glyphGraphics[i] = new Graphic();
            glyphGraphics[i].vertices = [
                new Vector2(0, 0), new Vector2(0, 0), new Vector2(0, 0),
                new Vector2(0, 0), new Vector2(0, 0), new Vector2(0, 0)
            ];
            glyphGraphics[i].texels = [
                new Vector2(0, 0), new Vector2(0, 0), new Vector2(0, 0),
                new Vector2(0, 0), new Vector2(0, 0), new Vector2(0, 0)
            ];
        }

        text = "";
        anchor = TextAnchor.LEFT_TOP;
        align = TextAlign.LEFT;

        pos = new Vector2(0, 0);
        scale = new Vector2(1, 1);
        angle = 0;
        color = 0xFF000000;
        depth = 0;

        cursor = 0;
        cursorX = 0;
        cursorY = 0;
        glyph = null;
        glyphX = 0;
        glyphY = 0;
        textWidth = 0;
        textLineWidths = [];
        textHeight = 0;
        lineIndex = 0;
        lineGlyphs = [];

        updateGlyphs();
    }

    public function updateGlyphs():Void
    {
        cursor = 0;
        cursorX = 0;
        cursorY = 0;
        glyph = null;
        glyphX = 0;
        glyphY = 0;
        textWidth = 0;
        textLineWidths = [0];
        textHeight = 0;
        lineIndex = 0;
        lineGlyphs = [[]];

        for (graphic in glyphGraphics)
        {
            graphic.vertices[0].setTo(0, 0);
            graphic.vertices[1].setTo(0, 0);
            graphic.vertices[2].setTo(0, 0);
            graphic.vertices[3].setTo(0, 0);
            graphic.vertices[4].setTo(0, 0);
            graphic.vertices[5].setTo(0, 0);
        }

        for (i in 0...Math.min(text.length, length))
        {
            if (text.charAt(i) == "\n")
            {
                cursorY += font.lineHeight;
                cursorX = 0;

                if (textLineWidths[lineIndex] > textWidth)
                {
                    textWidth = textLineWidths[lineIndex];
                }
                textHeight += font.lineHeight;

                lineIndex += 1;
                textLineWidths[lineIndex] = 0;
                lineGlyphs.push([]);
                continue;
            }

            glyph = font.glyphs[text.charCodeAt(i)];

            lineGlyphs[lineIndex].push(glyphGraphics[cursor]);

            glyphGraphics[cursor].pos.x = cursorX + glyph.xOffset;
            glyphGraphics[cursor].pos.y = cursorY + glyph.yOffset;

            cursorX += glyph.xAdvance;
            textLineWidths[lineIndex] += glyph.xAdvance;

            glyphGraphics[cursor].vertices[0].setTo(0, 0);
            glyphGraphics[cursor].vertices[1].setTo(glyph.width, 0);
            glyphGraphics[cursor].vertices[2].setTo(0, glyph.height);
            glyphGraphics[cursor].vertices[3].setTo(glyph.width, 0);
            glyphGraphics[cursor].vertices[4].setTo(glyph.width, glyph.height);
            glyphGraphics[cursor].vertices[5].setTo(0, glyph.height);

            glyphGraphics[cursor].texels[0].setTo(
                glyph.x / font.bitmapWidth,
                glyph.y / font.bitmapHeight);
            glyphGraphics[cursor].texels[1].setTo(
                (glyph.x + glyph.width) / font.bitmapWidth,
                glyph.y / font.bitmapHeight);
            glyphGraphics[cursor].texels[2].setTo(
                glyph.x / font.bitmapWidth,
                (glyph.y + glyph.height) / font.bitmapHeight);
            glyphGraphics[cursor].texels[3].setTo(
                (glyph.x + glyph.width) / font.bitmapWidth,
                glyph.y / font.bitmapHeight);
            glyphGraphics[cursor].texels[4].setTo(
                (glyph.x + glyph.width) / font.bitmapWidth,
                (glyph.y + glyph.height) / font.bitmapHeight);
            glyphGraphics[cursor].texels[5].setTo(
                glyph.x / font.bitmapWidth,
                (glyph.y + glyph.height) / font.bitmapHeight);

            cursor += 1;
        }

        if (textLineWidths[lineIndex] > textWidth)
        {
            textWidth = textLineWidths[lineIndex];
        }
        textHeight += font.lineHeight;

        for (i in 0...textLineWidths.length)
        {
            for (graphic in lineGlyphs[i])
            {
                switch align
                {
                    case TextAlign.LEFT:
                    // Don't do anything
                    case TextAlign.CENTER:
                    graphic.pos.x += (textWidth - textLineWidths[i]) / 2;
                    case TextAlign.RIGHT:
                    graphic.pos.x += textWidth - textLineWidths[i];
                }

                switch anchor
                {
                    case TextAnchor.LEFT_TOP:
                    // Don't do anything
                    case TextAnchor.CENTER_TOP:
                    graphic.pos.x -= textWidth / 2;
                    case TextAnchor.RIGHT_TOP:
                    graphic.pos.x -= textWidth;
                    case TextAnchor.LEFT_MIDDLE:
                    graphic.pos.y -= textHeight / 2;
                    case TextAnchor.CENTER_MIDDLE:
                    graphic.pos.x -= textWidth / 2;
                    graphic.pos.y -= textHeight / 2;
                    case TextAnchor.RIGHT_MIDDLE:
                    graphic.pos.x -= textWidth;
                    graphic.pos.y -= textHeight / 2;
                    case TextAnchor.LEFT_BOTTOM:
                    graphic.pos.y -= textHeight;
                    case TextAnchor.CENTER_BOTTOM:
                    graphic.pos.x -= textWidth / 2;
                    graphic.pos.y -= textHeight;
                    case TextAnchor.RIGHT_BOTTOM:
                    graphic.pos.x -= textWidth;
                    graphic.pos.y -= textHeight;
                }

                graphic.scale = scale;
                graphic.pos.x *= scale.x;
                graphic.pos.y *= scale.y;

                graphic.angle = angle;
                graphic.pos.setTo(
                    (graphic.pos.x * Math.cos(angle)) -
                    (graphic.pos.y * Math.sin(angle)),
                    (graphic.pos.x * Math.sin(angle)) +
                    (graphic.pos.y * Math.cos(angle)));

                graphic.pos.x += pos.x;
                graphic.pos.y += pos.y;

                graphic.color = color;
                graphic.depth = depth;
            }
        }
    }
}
