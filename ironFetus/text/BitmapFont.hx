package ironFetus.text;

import lime.utils.Assets;

import ironFetus.assets.Assets;
import ironFetus.assets.FontAsset;
import ironFetus.text.Glyph;


class BitmapFont
{
    public var face:String;
    public var size:Int;
    public var padding:{ up:Int, right:Int, down:Int, left:Int };
    public var spacing:{ horizontal:Int, vertical:Int };

    public var lineHeight:Int;
    public var base:Int;
    public var bitmapWidth:Int;
    public var bitmapHeight:Int;

    public var glyphs:Map<Int, Glyph>;

    public function new(fontAssetName:String)
    {
        face = "";
        size = 0;
        padding = { up: 0, right: 0, down: 0, left: 0 };
        spacing = { horizontal: 0, vertical: 0 };
        lineHeight = 0;
        base = 0;
        bitmapWidth = 0;
        bitmapHeight = 0;
        glyphs = new Map<Int, Glyph>();

        var fontAsset:FontAsset = Assets.fonts.get(fontAssetName);
        var lines:Array<String> = fontAsset.fontInfo.split("\n");
        for (line in lines)
        {
            var regex = ~/\s+/g;
            var splitLine:Array<String> = regex.split(line);
            if (splitLine.length == 0)
            {
                continue;
            }

            if (splitLine[0] == "info")
            {
                splitLine.shift();
                for (item in splitLine)
                {
                    var prop:Array<String> = item.split("=");
                    switch prop[0]
                    {
                        case "face": face = prop[1];
                        case "size": size = Std.parseInt(prop[1]);
                        case "padding":
                            var propItems:Array<String> = prop[1].split(",");
                            padding.up = Std.parseInt(propItems[0]);
                            padding.right = Std.parseInt(propItems[1]);
                            padding.down = Std.parseInt(propItems[2]);
                            padding.left = Std.parseInt(propItems[3]);
                        case "spacing":
                            var propItems:Array<String> = prop[1].split(",");
                            spacing.horizontal = Std.parseInt(propItems[0]);
                            spacing.vertical = Std.parseInt(propItems[1]);
                    }
                }
            }

            if (splitLine[0] == "common")
            {
                splitLine.shift();
                for (item in splitLine)
                {
                    var prop:Array<String> = item.split("=");
                    switch prop[0]
                    {
                        case "lineHeight": lineHeight = Std.parseInt(prop[1]);
                        case "base": base = Std.parseInt(prop[1]);
                        case "scaleW": bitmapWidth = Std.parseInt(prop[1]);
                        case "scaleH": bitmapHeight = Std.parseInt(prop[1]);
                    }
                }
            }

            if (splitLine[0] == "char")
            {
                splitLine.shift();
                var glyph = new Glyph();
                for (item in splitLine)
                {
                    var prop:Array<String> = item.split("=");
                    switch prop[0]
                    {
                        case "id": glyph.id = Std.parseInt(prop[1]);
                        case "x": glyph.x = Std.parseInt(prop[1]);
                        case "y": glyph.y = Std.parseInt(prop[1]);
                        case "width": glyph.width = Std.parseInt(prop[1]);
                        case "height": glyph.height = Std.parseInt(prop[1]);
                        case "xoffset": glyph.xOffset = Std.parseInt(prop[1]);
                        case "yoffset": glyph.yOffset = Std.parseInt(prop[1]);
                        case "xadvance": glyph.xAdvance = Std.parseInt(prop[1]);
                        case "page": glyph.page = Std.parseInt(prop[1]);
                        case "chnl": glyph.channel = Std.parseInt(prop[1]);
                    }
                }
                glyphs.set(glyph.id, glyph);
            }
        }
    }
}
