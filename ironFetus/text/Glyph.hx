package ironFetus.text;


class Glyph
{
    public var id:Int;
    public var x:Int;
    public var y:Int;
    public var width:Int;
    public var height:Int;
    public var xOffset:Int;
    public var yOffset:Int;
    public var xAdvance:Int;
    public var page:Int;
    public var channel:Int;

    public function new()
    {
        id = 0;
        x = 0;
        y = 0;
        width = 0;
        height = 0;
        xOffset = 0;
        yOffset = 0;
        xAdvance = 0;
        page = 0;
        channel = 0;
    }
}
