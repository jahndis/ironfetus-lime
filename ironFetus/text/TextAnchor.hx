package ironFetus.text;


enum TextAnchor
{
    LEFT_TOP;
    CENTER_TOP;
    RIGHT_TOP;
    LEFT_MIDDLE;
    CENTER_MIDDLE;
    RIGHT_MIDDLE;
    LEFT_BOTTOM;
    CENTER_BOTTOM;
    RIGHT_BOTTOM;
}
