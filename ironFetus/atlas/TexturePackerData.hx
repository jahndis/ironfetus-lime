package ironFetus.atlas;

import ironFetus.Vector2;


typedef Frame = {
    var x:Int;
    var y:Int;
    var width:Int;
    var height:Int;
    // TODO Add in these fields later if needed.
    //var rotated:Bool;
    //var trimmed:Bool;
    //var spriteSourceSizeX:Int;
    //var spriteSourceSizeY:Int;
    //var spriteSourceSizeWidth:Int;
    //var spriteSourceSizeHeight:Int;
    //var spriteSourceWidth:Int;
    //var spriteSourceHeight:Int;
    //var pivotX:Float;
    //var pivotY:Float;
};


class TexturePackerData
{
    public var version:String;
    public var width:Int;
    public var height:Int;
    public var scale:Float;
    public var frames:Map<String, Frame>;

    public function new(json:Dynamic)
    {
        version = json.meta.version;
        width = json.meta.size.w;
        height = json.meta.size.h;
        scale = json.meta.scale;

        frames = new Map<String, Frame>();
        var frame:Dynamic;
        for (name in Reflect.fields(json.frames))
        {
            frame = Reflect.field(json.frames, name);
            frames.set(name, {
                x: frame.frame.x,
                y: frame.frame.y,
                width: frame.frame.w,
                height: frame.frame.h,
            });
        }
    }
}
