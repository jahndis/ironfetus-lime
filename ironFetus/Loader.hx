package ironFetus;


class Loader
{
    private var currentTaskIndex:Int;
    private var tasks:Array<Void->Void>;
    private var onCompleteCallback:Void->Void;

    public function new()
    {
        currentTaskIndex = 0;
        tasks = [];
        onCompleteCallback = null;
    }

    public function getProgress():Float
    {
        return currentTaskIndex / tasks.length;
    }

    public function addTask(task:Void->Void):Void
    {
        tasks.push(task);
    }

    public function load():Int
    {
        tasks[currentTaskIndex]();
        currentTaskIndex += 1;
        if (currentTaskIndex == tasks.length)
        {
            onCompleteCallback();
        }
        return currentTaskIndex;
    }

    public function onComplete(callback:Void->Void):Void
    {
        onCompleteCallback = callback;
    }
}
