package ironFetus;

import lime.app.Application;
import lime.graphics.RenderContext;
import lime.system.System;
import lime.ui.KeyCode;
import lime.ui.KeyModifier;
import lime.ui.Touch in LimeTouch;

import ironFetus.Debug;
import ironFetus.Fe;
import ironFetus.Rectangle;
import ironFetus.assets.Assets;
import ironFetus.assets.FontAsset;
import ironFetus.assets.ImageAsset;
import ironFetus.assets.SoundAsset;
import ironFetus.assets.TextAsset;
import ironFetus.input.Key;
import ironFetus.input.Input;
import ironFetus.input.MouseButton;
import ironFetus.input.Touches;

#if neko
import neko.vm.Gc;
#end
#if cpp
import cpp.vm.Gc;
#end


class Game extends Application
{
    public var initialized:Bool;

    public var width:Int;
    public var height:Int;
    public var fullWidth:Int;
    public var fullHeight:Int;
    public var viewport:Rectangle;

    public var fixedFrameRate:Bool;
    public var updateFrameRate(default, set):Float;
    public var renderFrameRate(default, set):Float;

    private var deltaTime:Float;
    private var currentTime:Float;
    private var accumulatedTime:Float;
    private var newTime:Float;
    private var frameTime:Float;

    public function new(?w:Int=-1, ?h:Int=-1)
    {
        super();

        initialized = false;

        width = w;
        height = h;
        fullWidth = width;
        fullHeight = height;
        viewport = new Rectangle();

        Assets.images = new Map<String, ImageAsset>();
        Assets.sounds = new Map<String, SoundAsset>();
        Assets.fonts = new Map<String, FontAsset>();
        Assets.texts = new Map<String, TextAsset>();

        Fe.game = this;
        Fe.gl = null;
        Fe.window = null;
        Fe.input = null;
        Fe.sounds = null;
        Fe.random = null;
        Fe.scene = null;
    }

    public override function onWindowCreate():Void
    {
        super.onWindowCreate();

        fixedFrameRate = true;
        renderFrameRate = window.frameRate;
        updateFrameRate = window.frameRate;

        deltaTime = 1.0 / updateFrameRate;
        currentTime = System.getTimer() / 1000.0;
        accumulatedTime = 0;
        newTime = 0;
        frameTime = 0;

        switch (window.context.type)
        {
            case OPENGL | OPENGLES | WEBGL:
            Fe.gl = window.context;
            Fe.window = window;
            Fe.input = new Input();
            Fe.sounds = [];
            Fe.random = new Random();

            default:
            trace(window.context.type);
            trace("Invalid context");
            exit();
        }

        trace("Window Size", window.width, window.height);
        trace("Window Scale", window.scale);
        var preciseWidth:Float = width;
        var preciseHeight:Float = height;

        if (width == -1 && height == -1)
        {
            preciseWidth = window.width;
            preciseHeight = window.height;
        }
        else if (width == -1)
        {
            preciseWidth = (window.width * height) / window.height;
        }
        else if (height == -1)
        {
            preciseHeight = (window.height * width) / window.width;
        }

        width = Std.int(preciseWidth + 0.5);
        height = Std.int(preciseHeight + 0.5);
        trace("Game Size", width, height);

        var gameAspectRatio:Float = preciseWidth / preciseHeight;
        trace("Game Aspect Ratio", gameAspectRatio);
        var windowAspectRatio:Float = window.width / window.height;
        trace("Window Aspect Ratio", windowAspectRatio);

        if (gameAspectRatio >= windowAspectRatio)
        {
            fullWidth = Std.int(preciseWidth + 0.5);
            fullHeight = Std.int((preciseWidth / windowAspectRatio) + 0.5);
        }
        else
        {
            fullWidth = Std.int((preciseHeight * windowAspectRatio) + 0.5);
            fullHeight = Std.int(preciseHeight + 0.5);
        }
        trace("Game Full Size", fullWidth, fullHeight);

        viewport.width = Std.int((window.width * window.scale) + 0.5);
        viewport.height = Std.int((viewport.width / gameAspectRatio) + 0.5);
        if (viewport.height > Std.int(window.height * window.scale))
        {
            viewport.height = Std.int((window.height * window.scale) + 0.5);
            viewport.width = Std.int((viewport.height * gameAspectRatio) + 0.5);
        }
        viewport.x = Std.int((window.width * window.scale * 0.5) -
            (viewport.width * 0.5));
        viewport.y = Std.int((window.height * window.scale * 0.5) -
            (viewport.height * 0.5));
        trace("Viewport Rect", viewport.x, viewport.y, viewport.width,
            viewport.height);
    }

    public function set_updateFrameRate(newFrameRate:Float):Float
    {
        updateFrameRate = newFrameRate;
        deltaTime = 1.0 / updateFrameRate;
        return updateFrameRate;
    }

    public function set_renderFrameRate(newFrameRate:Float):Float
    {
        renderFrameRate = newFrameRate;
        window.frameRate = newFrameRate;
        return renderFrameRate;
    }

    public function init():Void
    {
        throw "Method not implemented";
    }

    public override function onPreloadComplete():Void
    {
        super.onPreloadComplete();

        init();
        initialized = true;
    }

    public override function update(delta:Int):Void
    {
        super.update(delta);

        if (initialized)
        {
            Debug.update(delta);

            if (fixedFrameRate)
            {
                newTime = System.getTimer() / 1000.0;
                frameTime = newTime - currentTime;
                if (frameTime > 0.25)
                {
                    frameTime = 0.25;
                }
                currentTime = newTime;
                accumulatedTime += frameTime;

                while (accumulatedTime >= deltaTime)
                {
                    Fe.scene.preUpdate(deltaTime);
                    Fe.scene.update(deltaTime);
                    Fe.scene.postUpdate(deltaTime);
                    accumulatedTime -= deltaTime;
                }
            }
            else
            {
                Fe.scene.preUpdate(delta / 1000.0);
                Fe.scene.update(delta / 1000.0);
                Fe.scene.postUpdate(delta / 1000.0);
            }
        }
    }

    public override function render(context:RenderContext):Void
    {
        super.render(context);

        if (initialized)
        {
            Fe.scene.render();
        }
    }

    public override function onMouseDown(x:Float, y:Float, button:Int):Void
    {
        x = ((x * fullWidth) / window.width) - ((fullWidth - width) * 0.5);
        y = ((y * fullHeight) / window.height) - ((fullHeight - height) * 0.5);

        if (Fe.input.mouse.buttons[button] != null)
        {
            if (Fe.input.mouse.buttons[button].up)
            {
                Fe.input.mouse.buttons[button].down = true;
                Fe.input.mouse.buttons[button].up = false;
                for (func in Fe.input.mouse.buttons[button].onDown)
                {
                    func(x, y);
                }
            }
        }
    }

    public override function onMouseUp(x:Float, y:Float, button:Int):Void
    {
        x = ((x * fullWidth) / window.width) - ((fullWidth - width) * 0.5);
        y = ((y * fullHeight) / window.height) - ((fullHeight - height) * 0.5);

        if (Fe.input.mouse.buttons[button] != null)
        {
            if (Fe.input.mouse.buttons[button].down)
            {
                Fe.input.mouse.buttons[button].down = false;
                Fe.input.mouse.buttons[button].up = true;
                for (func in Fe.input.mouse.buttons[button].onUp)
                {
                    func(x, y);
                }
            }
        }
    }

    public override function onMouseMove(x:Float, y:Float):Void
    {
        x = ((x * fullWidth) / window.width) - ((fullWidth - width) * 0.5);
        y = ((y * fullHeight) / window.height) - ((fullHeight - height) * 0.5);

        Fe.input.mouse.pos.setTo(x, y);
    }

    public override function onKeyDown(keyCode:KeyCode, mod:KeyModifier):Void
    {
        keyCode = Key.getKeyCode(keyCode);

        if (Fe.input.keyboard.keys[keyCode] != null)
        {
            if (Fe.input.keyboard.keys[keyCode].up)
            {
                Fe.input.keyboard.keys[keyCode].up = false;
                Fe.input.keyboard.keys[keyCode].down = true;
                for (func in Fe.input.keyboard.keys[keyCode].onDown)
                {
                    func();
                }
            }
        }
    }

    public override function onKeyUp(keyCode:KeyCode, mod:KeyModifier):Void
    {
        keyCode = Key.getKeyCode(keyCode);

        if (Fe.input.keyboard.keys[keyCode] != null)
        {
            if (Fe.input.keyboard.keys[keyCode].down)
            {
                Fe.input.keyboard.keys[keyCode].up = true;
                Fe.input.keyboard.keys[keyCode].down = false;
                for (func in Fe.input.keyboard.keys[keyCode].onUp)
                {
                    func();
                }
            }
        }
    }

    public override function onTouchStart(touch:LimeTouch):Void
    {
        Fe.input.touches.add(touch);
    }

    public override function onTouchEnd(touch:LimeTouch):Void
    {
        Fe.input.touches.remove(touch);
    }

    public function restart():Void
    {
        initialized = false;

        Fe.scene = null;
        Fe.input = null;
        var numSounds:Int = Fe.sounds.length;
        for (i in 0...numSounds)
        {
            Fe.sounds[numSounds - 1 - i].dispose();
        }
        Fe.sounds = null;
        Fe.random = null;

        Assets.dispose();

#if neko
        neko.vm.Gc.run(true);
#end
#if cpp
        cpp.vm.Gc.run(true);
        cpp.vm.Gc.compact();
#end

        Fe.input = new Input();
        Fe.sounds = [];
        Fe.random = new Random();

        init();
        initialized = true;
    }

    public function exit():Void
    {
        window.close();
    }
}
