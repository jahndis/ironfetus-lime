package ironFetus;

import haxe.Serializer;
import haxe.Unserializer;
import haxe.io.Path;
import lime.system.System;

#if (js && html5)
import js.Browser;
import js.html.Storage;
#end

#if sys
import sys.FileSystem;
import sys.io.File;
import sys.io.FileOutput;
#end


class Save
{
    public var name:String;
    public var path:String;
    public var data:Map<String, String>;

    private static var legalNameChars:Array<String> = [
        "a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k", "l", "m",
        "n", "o", "p", "q", "r", "s", "t", "u", "v", "w", "x", "y", "z",
        "0", "1", "2", "3", "4", "5", "6", "7", "8", "9",
        "_", "-"
    ];

    public function new(n:String, ?p:String=null)
    {
        name = n;
        path = p;
        data = new Map<String, String>();

        var validName = true;
        if (name == null || name == "")
        {
            validName = false;
        }
        else
        {
            for (i in 0...name.length)
            {
                if (legalNameChars.indexOf(name.charAt(i)) == -1)
                {
                    validName = false;
                    break;
                }
            }
        }

        if (!validName)
        {
            throw "Invalid Save Name";
            return;
        }

        if (path == null)
        {
#if (js && html5)
            path = Browser.window.location.href;
#else
            path = "";
#end
        }

        var encodedData:String = null;

        try
        {
#if (js && html5)
            var storage = Browser.getLocalStorage();
            if (storage != null)
            {
                encodedData = storage.getItem(path + ":" + name);
            }
#else
            var savePath = getSavePath();
            if (FileSystem.exists(savePath))
            {
                encodedData = File.getContent(savePath);
            }
#end
        }
        catch (error:Dynamic)
        {
            trace(error);
        }

        if (encodedData != null && encodedData != "")
        {
            try
            {
                var unserializer:Unserializer = new Unserializer(encodedData);
                data = unserializer.unserialize();
            }
            catch (error:Dynamic)
            {
                trace(error);
            }
        }
    }

    public function save(?onComplete:String->Void=null):Void
    {
        var encodedData = Serializer.run(data);

        try
        {
#if (js && html5)
            var storage:Storage = Browser.getLocalStorage();
            if (storage != null)
            {
                storage.removeItem(path + ":" + name);
                storage.setItem(path + ":" + name, encodedData);
            }
#else
            var savePath:String = getSavePath();
            var directory:String = Path.directory(savePath);

            if (!FileSystem.exists(directory))
            {
                FileSystem.createDirectory(directory);
            }
            
            var output:FileOutput = File.write(savePath, false);
            output.writeString(encodedData);
            output.close();
#end
        }
        catch (error:Dynamic)
        {
            trace("Openfl says pending but is this an error with file save?");
            if (onComplete != null)
            {
                onComplete("Some kind of error??");
            }
        }

        if (onComplete != null)
        {
            onComplete(null);
        }
    }

    public function clear():Void
    {
        data = new Map<String, String>();

        try
        {
#if (js && html5)
            var storage:Storage = Browser.getLocalStorage();

            if (storage != null)
            {
                storage.removeItem(path + ":" + name);
            }
#else
            var savePath:String = getSavePath();

            if (FileSystem.exists(savePath))
            {
                FileSystem.deleteFile(savePath);
            }
#end
        }
        catch (error:Dynamic)
        {
            // TODO Handle this?
        }
    }

    private function getSavePath():String
    {
        return Path.join([
            System.applicationStorageDirectory,
            path,
            name + ".save"
            ]);
    }
}
