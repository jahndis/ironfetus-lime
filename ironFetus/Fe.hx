package ironFetus;

import lime.graphics.Image;
import lime.graphics.WebGLRenderContext;
import lime.ui.Window;

import ironFetus.Game;
import ironFetus.Random;
import ironFetus.Scene;
import ironFetus.input.Input;


class Fe
{
    public static var game:Game;
    public static var gl:WebGLRenderContext;
    public static var window:Window;
    public static var input:Input;
    public static var sounds:Array<Sound>;
    public static var random:Random;
    public static var scene(default, set):Scene;

    public static function set_scene(s:Scene):Scene
    {
        if (scene != null)
        {
            scene.deactivate();
        }
        scene = s;
        if (scene != null)
        {
            scene.activate();
        }
        return scene;
    }
}
