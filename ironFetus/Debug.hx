package ironFetus;

import ironFetus.FPS;
import ironFetus.Math;

#if neko
import neko.vm.Gc;
#end
#if cpp
import cpp.vm.Gc;
#end


class Debug
{
	public static var fps:FPS = new FPS();
	public static var memory:Float = 0;
	public static var peakMemory:Float = 0;

	public static function update(delta:Int):Void
	{
		Debug.fps.update(delta);

		Debug.memory = Math.round(Debug.getMemory() / 1024 / 1024 * 100) / 100;
		if (Debug.memory > Debug.peakMemory)
		{
			Debug.peakMemory = Debug.memory;
		}
	}

	public static function getMemory():Int
    {
#if neko
        return Gc.stats().heap;
#end
#if cpp
        return untyped
            __global__.__hxcpp_gc_used_bytes();
#end
#if (js && html5)
        return untyped __js__ (
            "(window.performance && window.performance.memory) ? 
            window.performance.memory.usedJSHeapSize : 0");
#else
        return 0;
#end
    }
}
