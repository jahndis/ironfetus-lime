package ironFetus;

import lime.media.AudioBuffer;
import lime.media.AudioSource;
import lime.utils.Assets;

import ironFetus.Fe;
import ironFetus.assets.Assets;


class Sound
{
    public var buffer:AudioBuffer;
    public var source:AudioSource;
    public var isPlaying:Bool;

    public function new(soundAssetName:String)
    {
        buffer = Assets.sounds.get(soundAssetName).buffer;
        source = null;

        isPlaying = false;

        Fe.sounds.push(this);
    }

    public function play(loop:Bool=false, once:Bool=false, offset:Int=0):Void
    {
        if (once && isPlaying)
        {
            return;
        }

        if (isPlaying)
        {
            stop();
        }

        source = new AudioSource(buffer);
        source.offset = offset;
        source.onComplete.add(stop);

        if (loop)
        {
            source.loops = 0x3FFFFFFF;
        }

        source.play();

        isPlaying = true;
    }

    public function pause():Void
    {
        if (source == null)
        {
            return;
        }

        source.pause();
        
        isPlaying = false;
    }

    public function stop():Void
    {
        if (source == null)
        {
            return;
        }

        source.stop();
        source.onComplete.remove(stop);
        source.dispose();

        isPlaying = false;
    }

    public function dispose():Void
    {
        stop();
        Fe.sounds.remove(this);
    }
}
