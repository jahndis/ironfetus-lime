package ironFetus;

import ironFetus.Math;
import ironFetus.Rectangle;
import ironFetus.Vector2;

class Collision
{
    public static function getLineIntersection(p1:Vector2, q1:Vector2,
        p2:Vector2, q2:Vector2, ?asSegment:Bool=true,
        ?intersection:Vector2=null):Vector2
    {
        var z1:Float = p1.x - q1.x;
        var z2:Float = p2.x - q2.x;
        var z3:Float = p1.y - q1.y;
        var z4:Float = p2.y - q2.y;
        var d:Float = (z1 * z4) - (z3 * z2);

        if (d == 0)
        {
            return null;
        }

        var pre:Float = (p1.x * q1.y) - (p1.y * q1.x);
        var post:Float = (p2.x * q2.y) - (p2.y * q2.x);
        var x:Float = ((pre * z2) - (z1 * post)) / d;
        var y:Float = ((pre * z4) - (z3 * post)) / d;

        if (asSegment)
        {
            if (x < Math.minf(p1.x, q1.x) || x > Math.maxf(p1.x, q1.x) ||
                x < Math.minf(p2.x, q2.x) || x > Math.maxf(p2.x, q2.x))
            {
                return null;
            }
            if (y < Math.minf(p1.y, q1.y) || y > Math.maxf(p1.y, q1.y) ||
                y < Math.minf(p2.y, q2.y) || y > Math.maxf(p2.y, q2.y))
            {
                return null;
            }
        }

        if (intersection == null)
        {
            intersection = new Vector2();
        }

        intersection.setTo(x, y);

        return intersection;
    }

    public static function lineIntersectsLine(p1:Vector2, q1:Vector2,
        p2:Vector2, q2:Vector2):Bool
    {
        var o1:Int = Math.orientation(p1, q1, p2);
        var o2:Int = Math.orientation(p1, q1, q2);
        var o3:Int = Math.orientation(p2, q2, p1);
        var o4:Int = Math.orientation(p2, q2, q1);

        if (o1 != o2 && o3 != o4)
        {
            return true;
        }

        if ((o1 == 0 && pointInSegment(p2, p1, q1)) ||
            (o2 == 0 && pointInSegment(q2, p1, q1)) ||
            (o3 == 0 && pointInSegment(p1, p2, q2)) ||
            (o4 == 0 && pointInSegment(q1, p2, q2)))
        {
            return true;
        }

        return false;
    }

    public static function pointInRectangle(p:Vector2, r:Rectangle):Bool
    {
        return p.x > r.left && p.x < r.right && p.y > r.top && p.y < r.bottom;
    }

    public static function pointInPolygon(p:Vector2,
        polygon:Array<Vector2>):Bool
    {
        var j:Int = polygon.length - 1;
        var inPolygon:Bool = false;
        for (i in 0...polygon.length)
        {
            if (((polygon[i].y > p.y) != (polygon[j].y > p.y)) &&
                (p.x < (polygon[j].x - polygon[i].x) * (p.y - polygon[i].y) /
                    (polygon[j].y - polygon[i].y) + polygon[i].x))
            {
                inPolygon = !inPolygon;
            }

            j = i;
        }

        return inPolygon;
    }

    public static function pointInSegment(p:Vector2, a:Vector2, b:Vector2):Bool
    {
        if (p.x <= Math.maxf(a.x, b.x) && p.x >= Math.minf(a.x, b.x) &&
            p.y <= Math.maxf(a.y, b.y) && p.y >= Math.minf(a.y, b.y))
        {
            return true;
        }

        return false;
    }

    public static function lineIntersectsPolygon(p:Vector2, q:Vector2,
        polygon:Array<Vector2>):Bool
    {
        for (i in 1...polygon.length)
        {
            if (lineIntersectsLine(p, q, polygon[i], polygon[i - 1]))
            {
                return true;
            }
        }

        return false;
    }
}
