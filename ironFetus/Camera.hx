package ironFetus;

import ironFetus.Fe;
import ironFetus.Rectangle;


class Camera
{
    public var port:Rectangle;
    public var view:Rectangle;

    public function new()
    {
        port = new Rectangle(0, 0, Fe.game.viewport.width,
            Fe.game.viewport.height);
        view = new Rectangle(0, 0, Fe.game.width, Fe.game.height);
    }
}
