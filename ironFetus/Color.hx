package ironFetus;

import ironFetus.Math;


class Color
{
    public static function createHSV(h:Float, s:Float, v:Float, ?a:Float=1):Int
    {
        var r:Float = 0;
        var g:Float = 0;
        var b:Float = 0;
        var i:Int = Math.floor(h * 6);
        var f:Float = h * 6 - i;
        var p:Float = v * (1 - s);
        var q:Float = v * (1 - (f * s));
        var t:Float = v * (1 - ((1 - f) * s));

        switch (i % 6)
        {
            case 0:
            r = v;
            g = t;
            b = p;
            case 1:
            r = q;
            g = v;
            b = p;
            case 2:
            r = p;
            g = v;
            b = t;
            case 3:
            r = p;
            g = q;
            b = v;
            case 4:
            r = t;
            g = p;
            b = v;
            case 5:
            r = v;
            g = p;
            b = q;
        }

        return createRGB(Std.int(r * 255), Std.int(g * 255), Std.int(b * 255),
            Std.int(a * 255));
    }

    public static function createRGB(r:Int, g:Int, b:Int, ?a:Int=255):Int
    {
        return ((a & 0xFF) << 24) |
            ((r & 0xFF) << 16) |
            ((g & 0xFF) << 8) |
            (b & 0xFF);
    }

    public static function addRGB(color:Int, red:Int, green:Int, blue:Int,
        ?alpha:Int=0):Int
    {
        return createRGB(
            r(color) + red,
            g(color) + green,
            b(color) + blue,
            a(color) + alpha);
    }

    public static function lerp(color1:Int, color2:Int, t:Float):Int
    {
        return (addRGB(color1,
            Std.int((r(color2) - r(color1)) * t),
            Std.int((g(color2) - g(color1)) * t),
            Std.int((b(color2) - b(color1)) * t),
            Std.int((a(color2) - a(color1)) * t)));
    }

    public static function a(color:Int):Int
    {
        return (color >> 24) & 0xFF;
    }

    public static function r(color:Int):Int
    {
        return (color >> 16) & 0xFF;
    }

    public static function g(color:Int):Int
    {
        return (color >> 8) & 0xFF;
    }

    public static function b(color:Int):Int
    {
        return color & 0xFF;
    }
}
