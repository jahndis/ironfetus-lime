package ironFetus.input;


class Keyboard
{
    public var keys:Map<Int, Key>;

    public function new()
    {
        keys = new Map<Int, Key>();
    }

    public function addKey(keyCode:Int):Key
    {
        if (keys.get(keyCode) == null)
        {
            keys.set(keyCode, new Key(keyCode));
        }

        return keys[keyCode];
    }

    public function getKey(keyCode:Int):Key
    {
        return keys[keyCode];
    }

    public function removeKey(keyCode:Int):Void
    {
        if (keys.get(keyCode) != null)
        {
            keys.remove(keyCode);
        }
    }

    public function removeAllKeys():Void
    {
        var keyCodes:Array<Int> = [for (code in keys.keys()) code];
        for (keyCode in keyCodes)
        {
            keys.remove(keyCode);
        }
    }
}
