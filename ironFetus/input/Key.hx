package ironFetus.input;

import lime.ui.KeyCode;


class Key
{
    public var code:Int;
    public var down:Bool;
    public var up:Bool;
    public var onDown:Array<Void->Void>;
    public var onUp:Array<Void->Void>;

    public function new(keyCode:Int)
    {
        code = keyCode;
        down = false;
        up = true;
        onDown = [];
        onUp = [];
    }

    public static function getKeyCode(code:Int):Int
    {
        switch code {
            case KeyCode.A: return Key.A;
            case KeyCode.B: return Key.B;
            case KeyCode.C: return Key.C;
            case KeyCode.D: return Key.D;
            case KeyCode.E: return Key.E;
            case KeyCode.F: return Key.F;
            case KeyCode.G: return Key.G;
            case KeyCode.H: return Key.H;
            case KeyCode.I: return Key.I;
            case KeyCode.J: return Key.J;
            case KeyCode.K: return Key.K;
            case KeyCode.L: return Key.L;
            case KeyCode.M: return Key.M;
            case KeyCode.N: return Key.N;
            case KeyCode.O: return Key.O;
            case KeyCode.P: return Key.P;
            case KeyCode.Q: return Key.Q;
            case KeyCode.R: return Key.R;
            case KeyCode.S: return Key.S;
            case KeyCode.T: return Key.T;
            case KeyCode.U: return Key.U;
            case KeyCode.V: return Key.V;
            case KeyCode.W: return Key.W;
            case KeyCode.X: return Key.X;
            case KeyCode.Y: return Key.Y;
            case KeyCode.Z: return Key.Z;

            case KeyCode.SPACE: return Key.SPACE;
            case KeyCode.ESCAPE: return Key.ESCAPE;
            case KeyCode.RETURN: return Key.RETURN;

            case KeyCode.RIGHT: return Key.RIGHT;
            case KeyCode.LEFT: return Key.LEFT;
            case KeyCode.DOWN: return Key.DOWN;
            case KeyCode.UP: return Key.UP;

            case KeyCode.APP_CONTROL_BACK: return Key.BACK;
        }

        trace("Keycode " + code + " not handled");
        return -1;
    }

    public static var A(default, never):Int = KeyCode.A;
    public static var B(default, never):Int = KeyCode.B;
    public static var C(default, never):Int = KeyCode.C;
    public static var D(default, never):Int = KeyCode.D;
    public static var E(default, never):Int = KeyCode.E;
    public static var F(default, never):Int = KeyCode.F;
    public static var G(default, never):Int = KeyCode.G;
    public static var H(default, never):Int = KeyCode.H;
    public static var I(default, never):Int = KeyCode.I;
    public static var J(default, never):Int = KeyCode.J;
    public static var K(default, never):Int = KeyCode.K;
    public static var L(default, never):Int = KeyCode.L;
    public static var M(default, never):Int = KeyCode.M;
    public static var N(default, never):Int = KeyCode.N;
    public static var O(default, never):Int = KeyCode.O;
    public static var P(default, never):Int = KeyCode.P;
    public static var Q(default, never):Int = KeyCode.Q;
    public static var R(default, never):Int = KeyCode.R;
    public static var S(default, never):Int = KeyCode.S;
    public static var T(default, never):Int = KeyCode.T;
    public static var U(default, never):Int = KeyCode.U;
    public static var V(default, never):Int = KeyCode.V;
    public static var W(default, never):Int = KeyCode.W;
    public static var X(default, never):Int = KeyCode.X;
    public static var Y(default, never):Int = KeyCode.Y;
    public static var Z(default, never):Int = KeyCode.Z;

    public static var SPACE(default, never):Int = KeyCode.SPACE;
    public static var ESCAPE(default, never):Int = KeyCode.ESCAPE;
    public static var RETURN(default, never):Int = KeyCode.RETURN;

    public static var RIGHT(default, never):Int = KeyCode.RIGHT;
    public static var LEFT(default, never):Int = KeyCode.LEFT;
    public static var DOWN(default, never):Int = KeyCode.DOWN;
    public static var UP(default, never):Int = KeyCode.UP;

    public static var BACK(default, never):Int = KeyCode.APP_CONTROL_BACK;
}
