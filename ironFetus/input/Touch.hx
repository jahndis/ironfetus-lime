package ironFetus.input;

import lime.ui.Touch in LimeTouch;

import ironFetus.Vector2;


class Touch
{
    private var touch:LimeTouch;
    private var _pos:Vector2;
    private var _delta:Vector2;
    private var _prev:Vector2;

    public var active:Bool;
    public var id(get, null):Int;
    public var pos(get, null):Vector2;
    public var start(default, null):Vector2;
    public var delta(get, null):Vector2;
    public var prev(get, null):Vector2;

    public function new()
    {
        touch = null;
        active = false;
        _pos = new Vector2();
        start = new Vector2();
        _delta = new Vector2();
        _prev = new Vector2();
    }

    public function create(t:LimeTouch):Void
    {
        active = true;
        touch = t;
        start.setTo(
            (t.x * Fe.game.fullWidth) -
                ((Fe.game.fullWidth - Fe.game.width) * 0.5),
            (t.y * Fe.game.fullHeight) -
                ((Fe.game.fullHeight - Fe.game.height) * 0.5));
    }

    public function destroy():Void
    {
        active = false;
        touch = null;
    }

    private function get_id():Int
    {
        return touch.id;
    }

    private function get_pos():Vector2
    {
        _pos.setTo(
            (touch.x * Fe.game.fullWidth) -
                ((Fe.game.fullWidth - Fe.game.width) * 0.5),
            (touch.y * Fe.game.fullHeight) -
                ((Fe.game.fullHeight - Fe.game.height) * 0.5));
        return _pos;
    }

    private function get_delta():Vector2
    {
        _delta.setTo(
            (touch.dx * Fe.game.fullWidth) -
                ((Fe.game.fullWidth - Fe.game.width) * 0.5),
            (touch.dy * Fe.game.fullHeight) -
                ((Fe.game.fullHeight - Fe.game.height) * 0.5));
        return _delta;
    }

    private function get_prev():Vector2
    {
        _prev.setTo(
            ((touch.x - touch.dx) * Fe.game.fullWidth) -
                ((Fe.game.fullWidth - Fe.game.width) * 0.5),
            ((touch.y - touch.dy) * Fe.game.fullHeight) -
                ((Fe.game.fullHeight - Fe.game.height) * 0.5));
        return _prev;
    }
}
