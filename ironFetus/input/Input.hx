package ironFetus.input;

import ironFetus.Vector2;


class Input
{
    public var mouse:Mouse;
    public var keyboard:Keyboard;
    public var touches:Touches;

    public function new()
    {
        mouse = new Mouse();
        keyboard = new Keyboard();
        touches = new Touches();
    }
}
