package ironFetus.input;

import lime.ui.Touch in LimeTouch;


class Touches
{
    private var touchesPool:Array<Touch> = [
        new Touch(), new Touch(), new Touch(), new Touch(), new Touch()
    ];
    private var touches:Array<Touch>;

    public var length(get, null):Int;
    public var down:Bool;
    public var up:Bool;
    public var onDown:Array<Float->Float->Void>;
    public var onUp:Array<Float->Float->Void>;

    public function new()
    {
        touches = [];
        down = false;
        up = true;
        onDown = [];
        onUp = [];
    }

    public function add(touch:LimeTouch):Void
    {
        if (touches.length >= touchesPool.length)
        {
            return;
        }

        for (t in touchesPool)
        {
            if (!t.active)
            {
                t.create(touch);

                touches.push(t);

                for (func in onDown)
                {
                    func(t.pos.x, t.pos.y);
                }

                down = true;
                up = false;

                break;
            }
        }
    }

    public function remove(touch:LimeTouch):Void
    {
        if (touches.length == 0)
        {
            return;
        }

        for (t in touches)
        {
            if (t.id == touch.id)
            {
                touches.remove(t);

                if (touches.length == 0)
                {
                    down = false;
                    up = true;
                }

                for (func in onUp)
                {
                    func(t.pos.x, t.pos.y);
                }

                t.destroy();

                break;
            }
        }
    }

    public function get(index:Int):Touch
    {
        if (index >= length)
        {
            return null;
        }
        return touches[index];
    }

    public function get_length():Int
    {
        return touches.length;
    }

    public function iterator():Iterator<Touch>
    {
        return touches.iterator();
    }
}
