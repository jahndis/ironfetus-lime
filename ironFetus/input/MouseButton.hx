package ironFetus.input;

class MouseButton
{
    public var code:Int;
    public var down:Bool;
    public var up:Bool;
    public var onDown:Array<Float->Float->Void>;
    public var onUp:Array<Float->Float->Void>;

    public function new(buttonCode:Int)
    {
        code = buttonCode;
        down = false;
        up = true;
        onDown = [];
        onUp = [];
    }

    public static var LEFT(default, never):Int = 0;
    public static var MIDDLE(default, never):Int = 1;
    public static var RIGHT(default, never):Int = 2;
}
