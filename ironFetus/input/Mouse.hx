package ironFetus.input;

import ironFetus.Vector2;
import ironFetus.input.MouseButton;

class Mouse
{
    public var pos:Vector2;
    public var buttons:Map<Int, MouseButton>;

    public function new()
    {
        pos = new Vector2();
        buttons = new Map<Int, MouseButton>();
    }

    public function addButton(buttonCode:Int):MouseButton
    {
        if (buttons.get(buttonCode) == null)
        {
            buttons.set(buttonCode, new MouseButton(buttonCode));
        }

        return buttons[buttonCode];
    }

    public function getButton(buttonCode:Int):MouseButton
    {
        return buttons[buttonCode];
    }

    public function removeButton(buttonCode:Int):Void
    {
        if (buttons.get(buttonCode) != null)
        {
            buttons.remove(buttonCode);
        }
    }

    public function removeAllButtons():Void
    {
        var buttonCodes:Array<Int> = [for (code in buttons.keys()) code];
        for (buttonCode in buttonCodes)
        {
            buttons.remove(buttonCode);
        }
    }
}
