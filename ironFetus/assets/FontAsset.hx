package ironFetus.assets;

import lime.utils.Assets;


class FontAsset
{
    public var fontInfo:String;
    public var path:String;

    public function new(p:String)
    {
        fontInfo = Assets.getText(p);
        path = p;
    }

    public function dispose():Void
    {
        fontInfo = null;
        Assets.cache.clear(path);
    }
}
