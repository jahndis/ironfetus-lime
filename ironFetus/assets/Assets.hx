package ironFetus.assets;

import lime.utils.Assets in LimeAssets;

import ironFetus.assets.FontAsset;
import ironFetus.assets.ImageAsset;
import ironFetus.assets.SoundAsset;
import ironFetus.assets.TextAsset;


class Assets
{
    public static var images:Map<String, ImageAsset>;
    public static var sounds:Map<String, SoundAsset>;
    public static var fonts:Map<String, FontAsset>;
    public static var texts:Map<String, TextAsset>;

    public static function loadImage(name:String, path:String):ImageAsset
    {
        var imageAsset:ImageAsset = images.get(name);
        if (imageAsset == null || imageAsset.path != path)
        {
            imageAsset = new ImageAsset(path);
            images.set(name, imageAsset);
        }
        return imageAsset;
    }

    public static function disposeImage(name:String):Void
    {
        var imageAsset:ImageAsset = images.get(name);
        if (imageAsset != null)
        {
            imageAsset.dispose();
            images.remove(name);
        }
    }

    public static function loadSound(name:String, path:String):SoundAsset
    {
        var soundAsset:SoundAsset = sounds.get(name);
        if (soundAsset == null || soundAsset.path != path)
        {
            soundAsset = new SoundAsset(path);
            sounds.set(name, soundAsset);
        }
        return soundAsset;
    }

    public static function disposeSound(name:String):Void
    {
        var soundAsset:SoundAsset = sounds.get(name);
        if (soundAsset != null)
        {
            soundAsset.dispose();
            sounds.remove(name);
        }
    }

    public static function loadFont(name:String, path:String):FontAsset
    {
        var fontAsset:FontAsset = fonts.get(name);
        if (fontAsset == null || fontAsset.path != path)
        {
            fontAsset = new FontAsset(path);
            fonts.set(name, fontAsset);
        }
        return fontAsset;
    }

    public static function disposeFont(name:String):Void
    {
        var fontAsset:FontAsset = fonts.get(name);
        if (fontAsset != null)
        {
            fontAsset.dispose();
            fonts.remove(name);
        }
    }

    public static function loadText(name:String, path:String):TextAsset
    {
        var textAsset:TextAsset = texts.get(name);
        if (textAsset == null || textAsset.path != path)
        {
            textAsset = new TextAsset(path);
            texts.set(name, textAsset);
        }
        return textAsset;
    }

    public static function disposeText(name:String):Void
    {
        var textAsset:TextAsset = texts.get(name);
        if (textAsset != null)
        {
            textAsset.dispose();
            texts.remove(name);
        }
    }

    public static function dispose():Void
    {
        var names:Array<String> = [for (name in images.keys()) name];
        for (name in names)
        {
            disposeImage(name);
        }
        names = [for (name in sounds.keys()) name];
        for (name in names)
        {
            disposeSound(name);
        }
        names = [for (name in fonts.keys()) name];
        for (name in names)
        {
            disposeFont(name);
        }
        names = [for (name in texts.keys()) name];
        for (name in names)
        {
            disposeText(name);
        }
    }
}
