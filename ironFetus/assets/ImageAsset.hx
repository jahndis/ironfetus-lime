package ironFetus.assets;

import lime.graphics.Image;
import lime.utils.Assets;


class ImageAsset
{
    public var image:Image;
    public var path:String;

    public function new(p:String)
    {
        image = Assets.getImage(p);
        path = p;
    }

    public function dispose():Void
    {
        image = null;
        Assets.cache.clear(path);
    }
}
