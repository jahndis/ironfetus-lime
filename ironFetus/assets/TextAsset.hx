package ironFetus.assets;

import lime.utils.Assets;


class TextAsset
{
    public var text:String;
    public var path:String;

    public function new(p:String)
    {
        text = Assets.getText(p);
        path = p;
    }

    public function dispose():Void
    {
        text = null;
    }
}
