package ironFetus.assets;

import lime.media.AudioBuffer;
import lime.utils.Assets;


class SoundAsset
{
    public var buffer:AudioBuffer;
    public var path:String;

    public function new(p:String)
    {
        buffer = Assets.getAudioBuffer(p);
        path = p;
    }

    public function dispose():Void
    {
        buffer = null;
        Assets.cache.clear(path);
    }
}
