package ironFetus;

import ironFetus.Vector2;


class Graphic
{
    public var pos:Vector2;
    public var scale:Vector2;
    public var angle:Float;
    public var color:Int;
    public var vertices:Array<Vector2>;
    public var texels:Array<Vector2>;
    public var depth:Int;

    public function new()
    {
        pos = new Vector2();
        scale = new Vector2(1, 1);
        angle = 0;
        color = 0xFFFFFFFF;
        vertices = [];
        texels = [];
        depth = 0;
    }

    public function clone():Graphic
    {
        var graphic:Graphic = new Graphic();

        graphic.pos = new Vector2(pos.x, pos.y);
        graphic.scale = new Vector2(scale.x, scale.y);
        graphic.angle = angle;
        graphic.color = color;

        for (vertex in vertices)
        {
            graphic.vertices.push(new Vector2(vertex.x, vertex.y));
        }

        for (texel in texels)
        {
            graphic.texels.push(new Vector2(texel.x, texel.y));
        }
        graphic.depth = depth;

        return graphic;
    }

    public static function getAlpha(g:Graphic):Int
    {
        return Color.a(g.color);
    }

    public static function setAlpha(g:Graphic, alpha:Int):Void
    {
        g.color = (alpha << 24) | (g.color & 0x00FFFFFF);
    }

    public static function changeAlpha(g:Graphic, alpha:Int):Void
    {
        var newAlpha:Int = Math.boundInt(getAlpha(g) + alpha, 0x0, 0xFF);

        g.color = (newAlpha << 24) | (g.color & 0x00FFFFFF);
    }

    public static function makeQuadVertices(g:Graphic, x1, y1, x2, y2):Void
    {
        g.vertices = [
            new Vector2(x1, y1), new Vector2(x2, y1), new Vector2(x2, y2),
            new Vector2(x1, y1), new Vector2(x2, y2), new Vector2(x1, y2)
        ];
    }

    public static function setQuadVertices(g:Graphic, x1, y1, x2, y2):Void
    {
        g.vertices[0].setTo(x1, y1);
        g.vertices[1].setTo(x2, y1);
        g.vertices[2].setTo(x2, y2);
        g.vertices[3].setTo(x1, y1);
        g.vertices[4].setTo(x2, y2);
        g.vertices[5].setTo(x1, y2);
    }

    public static function makeQuadTexels(g:Graphic, u1, v1, u2, v2):Void
    {
        g.texels = [
            new Vector2(u1, v1), new Vector2(u2, v1), new Vector2(u2, v2),
            new Vector2(u1, v1), new Vector2(u2, v2), new Vector2(u1, v2)
        ];
    }

    public static function setQuadTexels(g:Graphic, u1, v1, u2, v2):Void
    {
        g.texels[0].setTo(u1, v1);
        g.texels[1].setTo(u2, v1);
        g.texels[2].setTo(u2, v2);
        g.texels[3].setTo(u1, v1);
        g.texels[4].setTo(u2, v2);
        g.texels[5].setTo(u1, v2);
    }
}
