package ironFetus.rendering;

import lime.graphics.opengl.GLBuffer;
import lime.graphics.opengl.GLProgram;
import lime.graphics.opengl.GLTexture;
import lime.utils.Float32Array;

import ironFetus.Camera;
import ironFetus.Fe.gl;
import ironFetus.Graphic;
import ironFetus.assets.Assets;
import ironFetus.assets.ImageAsset;

class Renderer
{
    public var active:Bool;

    public var camera:Camera;
    public var graphics:Array<Graphic>;
    public var numVertices:Int;

    public var width:Int;
    public var height:Int;
    public var texture:GLTexture;

    public var sortFunction:Graphic->Graphic->Int;

    private var program:GLProgram;
    private var vertexShader:String;
    private var fragmentShader:String;

    private var staticBuffer:GLBuffer;
    private var staticData:Float32Array;
    private var dynamicBuffer:GLBuffer;
    private var dynamicData:Float32Array;

    public function new(?imageAssetName:String)
    {
        active = true;

        camera = null;
        graphics = [];
        numVertices = 0;

		width = 0;
		height = 0;
        texture = null;

        sortFunction = defaultSortFunction;

        if (imageAssetName != null)
        {
            var imageAsset:ImageAsset = Assets.images.get(imageAssetName);
			width = imageAsset.image.buffer.width;
			height = imageAsset.image.buffer.height;

			texture = gl.createTexture(); 
			gl.bindTexture(gl.TEXTURE_2D, texture);
			gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_WRAP_S, gl.CLAMP_TO_EDGE);
			gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_WRAP_T, gl.CLAMP_TO_EDGE);
			gl.texImage2D(gl.TEXTURE_2D, 0, gl.RGBA, width, height, 0, gl.RGBA,
				gl.UNSIGNED_BYTE, imageAsset.image.data);
			gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MAG_FILTER, gl.NEAREST);
			gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MIN_FILTER, gl.NEAREST);
        }

        defineShaders();
        program = GLProgram.fromSources(gl, vertexShader, fragmentShader);
        gl.useProgram(program);

        defineShaderUniforms();
        defineShaderAttributes();

        staticBuffer = gl.createBuffer();
        dynamicBuffer = gl.createBuffer();
    }

    public function init():Void
    {
        updateStaticShaderUniforms();

        gl.bindBuffer(gl.ARRAY_BUFFER, staticBuffer);
        bindStaticShaderAttributes();
        defineStaticBufferData();
        if (staticData != null && staticData.length > 0)
        {
            updateStaticBufferData();
            gl.bufferData(gl.ARRAY_BUFFER, staticData, gl.STATIC_DRAW);
        }

        gl.bindBuffer(gl.ARRAY_BUFFER, dynamicBuffer);
        bindDynamicShaderAttributes();
        defineDynamicBufferData();

        numVertices = 0;
        for (graphic in graphics)
        {
            numVertices += graphic.vertices.length;
        }
    }

    public function update():Void
    {
        if (active && dynamicData != null && dynamicData.length > 0)
        {
            updateDynamicBufferData();
        }
    }

	public function render(cam:Camera):Void
    {
        if (active)
        {
            camera = cam;

            if (gl.getParameter(gl.CURRENT_PROGRAM) != program)
            {
                gl.useProgram(program);
            }
            if (texture != null)
            {
                if (gl.getParameter(gl.TEXTURE_BINDING_2D) != texture)
                {
                    gl.bindTexture(gl.TEXTURE_2D, texture);
                }
            }

            updateDynamicShaderUniforms();

            gl.bindBuffer(gl.ARRAY_BUFFER, staticBuffer);
            bindStaticShaderAttributes();

            gl.bindBuffer(gl.ARRAY_BUFFER, dynamicBuffer);
            bindDynamicShaderAttributes();

            if (dynamicData != null && dynamicData.length > 0)
            {
                gl.bufferData(gl.ARRAY_BUFFER, dynamicData, gl.DYNAMIC_DRAW);
            }

            gl.viewport(
                Std.int(Fe.game.viewport.x + camera.port.x),
                Std.int(Fe.game.viewport.y + Fe.game.viewport.height -
                    camera.port.y - camera.port.height),
                Std.int(camera.port.width),
                Std.int(camera.port.height));

            gl.drawArrays(gl.TRIANGLES, 0, numVertices);
        }
    }

    public function dispose():Void
    {
        active = false;

        gl.bindBuffer(gl.ARRAY_BUFFER, null);

        staticData = null;
        dynamicData = null;

        gl.deleteBuffer(staticBuffer);
        gl.deleteBuffer(dynamicBuffer);

        if (texture != null)
        {
            gl.bindTexture(gl.TEXTURE_2D, null);
            gl.deleteTexture(texture);
        }

        gl.deleteProgram(program);

        camera = null;
        graphics = [];
    }

    public function defaultSortFunction(a:Graphic, b:Graphic):Int
    {
        return a.depth < b.depth ? 1 : a.depth > b.depth ? -1 : 0;
    }

    public function sort():Void
    {
        graphics.sort(sortFunction);
    }

    public function addGraphic(graphic:Graphic):Void
    {
        graphics.push(graphic);
        init();
    }

    public function addGraphics(newGraphics:Array<Graphic>):Void
    {
        for (graphic in newGraphics)
        {
            graphics.push(graphic);
        }
        init();
    }

    private function defineShaders():Void
    {
        throw "Method not implemented";
    }

    private function defineShaderUniforms():Void
    {
        throw "Method not implemented";
    }

    private function defineShaderAttributes():Void
    {
        throw "Method not implemented";
    }

    private function defineStaticBufferData():Void
    {
        throw "Method not implemented";
    }

    private function defineDynamicBufferData():Void
    {
        throw "Method not implemented";
    }

    private function updateStaticBufferData():Void
    {
        throw "Method not implemented";
    }

    private function updateDynamicBufferData():Void
    {
        throw "Method not implemented";
    }

    private function updateStaticShaderUniforms():Void
    {
        throw "Method not implemented";
    }

    private function updateDynamicShaderUniforms():Void
    {
        throw "Method not implemented";
    }

    private function bindStaticShaderAttributes():Void
    {
        throw "Method not implemented";
    }

    private function bindDynamicShaderAttributes():Void
    {
        throw "Method not implemented";
    }
}
