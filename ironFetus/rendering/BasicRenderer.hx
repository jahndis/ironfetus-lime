package ironFetus.rendering;

import lime.graphics.opengl.GLUniformLocation;
import lime.utils.Float32Array;

import ironFetus.Color;
import ironFetus.Matrix3;
import ironFetus.Matrix4;
import ironFetus.Fe.gl;
import ironFetus.rendering.Renderer;

enum BasicRendererAttribute {
    TRANSFORM;
    TEXELS;
    COLOR;
}

class BasicRenderer extends Renderer
{
    private var matrixUniform:GLUniformLocation;
    private var imageUniform:GLUniformLocation;

    private var vertexAttribute:Int;
    private var textureAttribute:Int;
    private var colorAttribute:Int;

    private var transformIsDynamic:Bool;
    private var texelsIsDynamic:Bool;
    private var colorIsDynamic:Bool;

    private var staticAttributeStride:Int;
    private var dynamicAttributeStride:Int;

   	private var offset:Int;
    private var matrix:Matrix4;
    private var m:Matrix3;
    private var r:Float;
    private var g:Float;
    private var b:Float;
    private var a:Float;

    public function new(?imageAssetName:String,
        ?dynamicAttributes:Array<BasicRendererAttribute>)
    {
        if (dynamicAttributes == null)
        {
            dynamicAttributes = [];
        }

        transformIsDynamic = dynamicAttributes.indexOf(TRANSFORM) >= 0;
        texelsIsDynamic = (imageAssetName != null &&
            dynamicAttributes.indexOf(TEXELS) >= 0);
        colorIsDynamic = dynamicAttributes.indexOf(COLOR) >= 0;

        staticAttributeStride = 0;
        dynamicAttributeStride = 0;
        if (transformIsDynamic)
        {
            dynamicAttributeStride += 2;
        }
        else
        {
            staticAttributeStride += 2;
        }
        if (texelsIsDynamic)
        {
            dynamicAttributeStride += 2;
        }
        else
        {
            staticAttributeStride += 2;
        }
        if (colorIsDynamic)
        {
            dynamicAttributeStride += 4;
        }
        else
        {
            staticAttributeStride += 4;
        }

        super(imageAssetName);

        offset = 0;
        matrix = new Matrix4();
        m = new Matrix3();
        r = 1;
        g = 1;
        b = 1;
        a = 1;
    }

    public override function defineShaders():Void
    {
        if (texture == null)
        {
            vertexShader = "
                attribute vec2 aVertexPosition;
                attribute vec4 aColor;
                uniform mat4 uMatrix;
                varying vec4 vColor;
                
                void main (void) {
                    vColor = aColor;
                    gl_Position = uMatrix * vec4(aVertexPosition, 0.0, 1.0);
                }";

            fragmentShader =
                #if (!desktop)
                "precision mediump float;" +
                #end
                "varying vec4 vColor;
                
                void main (void) {
                    gl_FragColor = vColor;
                }";
        }
        else
        {
            vertexShader = "
                attribute vec2 aVertexPosition;
                attribute vec2 aTexCoord;
                attribute vec4 aColor;
                uniform mat4 uMatrix;
                varying vec2 vTexCoord;
                varying vec4 vColor;
                
                void main (void) {
                    vTexCoord = aTexCoord;
                    vColor = aColor;
                    gl_Position = uMatrix * vec4(aVertexPosition, 0.0, 1.0);
                }";

            fragmentShader =
                #if (!desktop)
                "precision mediump float;" +
                #end
                "varying vec2 vTexCoord;
                varying vec4 vColor;
                uniform sampler2D uImage0;
                
                void main (void) {
                    gl_FragColor = texture2D(uImage0, vTexCoord) * vColor;
                }";
        }
    }

    public override function defineShaderUniforms():Void
    {
        if (texture != null)
        {
            imageUniform = gl.getUniformLocation(program, "uImage0");
        }

        matrixUniform = gl.getUniformLocation(program, "uMatrix");
    }

    public override function defineShaderAttributes():Void
    {
        vertexAttribute = gl.getAttribLocation(program, "aVertexPosition");
        gl.enableVertexAttribArray(vertexAttribute);

        if (texture != null)
        {
            textureAttribute = gl.getAttribLocation(program, "aTexCoord");
            gl.enableVertexAttribArray(textureAttribute);
        }

        colorAttribute = gl.getAttribLocation(program, "aColor");
        gl.enableVertexAttribArray(colorAttribute);
    }

    public override function defineStaticBufferData():Void
    {
        var bufferSize:Int = 0;
        var graphicSize:Int = 0;
        for (graphic in graphics)
        {
            graphicSize = 0;
            if (!transformIsDynamic)
            {
                graphicSize += graphic.vertices.length * 2;
            }
            if (texture != null && !texelsIsDynamic)
            {
                graphicSize += graphic.vertices.length * 2;
            }
            if (!colorIsDynamic)
            {
                graphicSize += graphic.vertices.length * 4;
            }
            bufferSize += graphicSize;
        }
        staticData = new Float32Array(bufferSize);
    }

    public override function defineDynamicBufferData():Void
    {
        var bufferSize:Int = 0;
        var graphicSize:Int = 0;
        for (graphic in graphics)
        {
            graphicSize = 0;
            if (transformIsDynamic)
            {
                graphicSize += graphic.vertices.length * 2;
            }
            if (texture != null && texelsIsDynamic)
            {
                graphicSize += graphic.vertices.length * 2;
            }
            if (colorIsDynamic)
            {
                graphicSize += graphic.vertices.length * 4;
            }
            bufferSize += graphicSize;
        }
        dynamicData = new Float32Array(bufferSize);
    }

    public override function updateStaticBufferData():Void
    {
        offset = 0;

        for (graphic in graphics)
        {
            if (!transformIsDynamic)
            {
                m.identity();
                if (graphic.scale.x != 1 || graphic.scale.y != 1)
                {
                    m.scale(graphic.scale.x, graphic.scale.y);
                }
                if (graphic.angle != 0)
                {
                    m.rotate(graphic.angle);
                }   
                m.translate(graphic.pos.x, graphic.pos.y);
            }

            if (!colorIsDynamic)
            {
                r = Color.r(graphic.color) / 0xFF;
                g = Color.g(graphic.color) / 0xFF;
                b = Color.b(graphic.color) / 0xFF;
                a = Color.a(graphic.color) / 0xFF;
            }

            for (i in 0...graphic.vertices.length)
            {
                if (!transformIsDynamic)
                {
                    staticData[offset++] = (graphic.vertices[i].x * m.a) +
                        (graphic.vertices[i].y * m.c) + m.tx;
                    staticData[offset++] = (graphic.vertices[i].x * m.b) +
                        (graphic.vertices[i].y * m.d) + m.ty;
                }

                if (texture != null && !texelsIsDynamic)
                {
                    staticData[offset++] = graphic.texels[i].x;
                    staticData[offset++] = graphic.texels[i].y;
                }

                if (!colorIsDynamic)
                {
                    staticData[offset++] = r;
                    staticData[offset++] = g;
                    staticData[offset++] = b;
                    staticData[offset++] = a;
                }
            }
        }
    }

    public override function updateDynamicBufferData():Void
    {
        offset = 0;

        for (graphic in graphics)
        {
            if (transformIsDynamic)
            {
                m.identity();
                if (graphic.scale.x != 1 || graphic.scale.y != 1)
                {
                    m.scale(graphic.scale.x, graphic.scale.y);
                }
                if (graphic.angle != 0)
                {
                    m.rotate(graphic.angle);
                }   
                m.translate(graphic.pos.x, graphic.pos.y);
            }

            if (colorIsDynamic)
            {
                r = Color.r(graphic.color) / 0xFF;
                g = Color.g(graphic.color) / 0xFF;
                b = Color.b(graphic.color) / 0xFF;
                a = Color.a(graphic.color) / 0xFF;
            }

            for (i in 0...graphic.vertices.length)
            {
                if (transformIsDynamic)
                {
                    dynamicData[offset++] = (graphic.vertices[i].x * m.a) +
                        (graphic.vertices[i].y * m.c) + m.tx;
                    dynamicData[offset++] = (graphic.vertices[i].x * m.b) +
                        (graphic.vertices[i].y * m.d) + m.ty;
                }

                if (texture != null && texelsIsDynamic)
                {
                    dynamicData[offset++] = graphic.texels[i].x;
                    dynamicData[offset++] = graphic.texels[i].y;
                }

                if (colorIsDynamic)
                {
                    dynamicData[offset++] = r;
                    dynamicData[offset++] = g;
                    dynamicData[offset++] = b;
                    dynamicData[offset++] = a;
                }
            }
        }
    }

    public override function updateStaticShaderUniforms():Void
    {
        if (texture != null)
        {
            gl.uniform1i(imageUniform, 0);
        }
    }

    public override function updateDynamicShaderUniforms():Void
    {
        matrix.createOrtho(
            camera.view.x, camera.view.x + camera.view.width,
            camera.view.y + camera.view.height, camera.view.y, -1, 1);


        gl.uniformMatrix4fv(matrixUniform, false, matrix);
    }

    public override function bindStaticShaderAttributes():Void
    {
        offset = 0;
        if (!transformIsDynamic)
        {
            gl.vertexAttribPointer(vertexAttribute, 2, gl.FLOAT, false,
                staticAttributeStride * Float32Array.BYTES_PER_ELEMENT,
                offset * Float32Array.BYTES_PER_ELEMENT);
            offset += 2;
        }
        if (texture != null && !texelsIsDynamic)
        {
            gl.vertexAttribPointer(textureAttribute, 2, gl.FLOAT, false,
                staticAttributeStride * Float32Array.BYTES_PER_ELEMENT,
                offset * Float32Array.BYTES_PER_ELEMENT);
            offset += 2;
        }
        if (!colorIsDynamic)
        {
            gl.vertexAttribPointer(colorAttribute, 4, gl.FLOAT, false,
                staticAttributeStride * Float32Array.BYTES_PER_ELEMENT,
                offset * Float32Array.BYTES_PER_ELEMENT);
            offset += 4;
        }
    }

    public override function bindDynamicShaderAttributes():Void
    {
        offset = 0;
        if (transformIsDynamic)
        {
            gl.vertexAttribPointer(vertexAttribute, 2, gl.FLOAT, false,
                dynamicAttributeStride * Float32Array.BYTES_PER_ELEMENT,
                offset * Float32Array.BYTES_PER_ELEMENT);
            offset += 2;
        }
        if (texture != null && texelsIsDynamic)
        {
            gl.vertexAttribPointer(textureAttribute, 2, gl.FLOAT, false,
                dynamicAttributeStride * Float32Array.BYTES_PER_ELEMENT,
                offset * Float32Array.BYTES_PER_ELEMENT);
            offset += 2;
        }
        if (colorIsDynamic)
        {
            gl.vertexAttribPointer(colorAttribute, 4, gl.FLOAT, false,
                dynamicAttributeStride * Float32Array.BYTES_PER_ELEMENT,
                offset * Float32Array.BYTES_PER_ELEMENT);
            offset += 4;
        }
    }
}
