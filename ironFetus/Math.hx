package ironFetus;

import Math in HaxeMath;

import ironFetus.Vector2;


class Math
{
    public static var NEGATIVE_INFINITY(default, null):Float =
        HaxeMath.NEGATIVE_INFINITY;
    public static var PI(default, null):Float = HaxeMath.PI;
    public static var TWO_PI(default, null):Float = HaxeMath.PI * 2;
    public static var POSITIVE_INFINITY(default, null):Float =
        HaxeMath.POSITIVE_INFINITY;
    public static var MIN_INT:Int = -0x7FFFFFFF;
    public static var MAX_INT:Int = 0x7FFFFFFF;
    public static var EPSILON:Float = 0.0000001;

    public static function abs(value:Float):Float
    {
        return HaxeMath.abs(value);
    }

    public static function acos(angle:Float):Float
    {
        return HaxeMath.acos(angle);
    }

    public static function asin(angle:Float):Float
    {
        return HaxeMath.asin(angle);
    }

    public static function atan(angle:Float):Float
    {
        return HaxeMath.atan(angle);
    }

    public static function atan2(x:Float, y:Float):Float
    {
        return HaxeMath.atan2(x, y);
    }

    public static function boundFloat(value:Float, min:Float, max:Float):Float
    {
        return value < min ? min : value > max ? max : value;
    }

    public static function boundInt(value:Int, min:Int, max:Int):Int
    {
        return value < min ? min : value > max ? max : value;
    }

    public static function ceil(value:Float):Int
    {
        return HaxeMath.ceil(value);
    }

    public static function cos(angle:Float):Float
    {
        return HaxeMath.cos(angle);
    }

    public static function cube(value:Float):Float
    {
        return value * value * value;
    }

    public static function distanceSquared(p:Vector2,
        q:Vector2):Float
    {
        return ((q.x - p.x) * (q.x - p.x)) + ((q.y - p.y) * (q.y - p.y));
    }

    public static function distance(p:Vector2, q:Vector2):Float
    {
        return HaxeMath.sqrt(
            ((q.x - p.x) * (q.x - p.x)) + ((q.y - p.y) * (q.y - p.y)));
    }

    public static function dotProduct(p:Vector2, q:Vector2):Float
    {
        return (p.x * q.x) + (p.y * q.y);
    }

    public static function floor(value:Float):Int
    {
        return HaxeMath.floor(value);
    }

    public static function max(a:Int, b:Int):Int
    {
        return a >= b ? a : b;
    }

    public static function maxf(a:Float, b:Float):Float
    {
        return a >= b ? a : b;
    }

    public static function min(a:Int, b:Int):Int
    {
        return a <= b ? a : b;
    }

    public static function minf(a:Float, b:Float):Float
    {
        return a <= b ? a : b;
    }

    public static function orientation(a:Vector2, b:Vector2, c:Vector2):Int
    {
        var o:Int = Std.int(((b.y - a.y) * (c.x - b.x)) -
            ((b.x - a.x) * (c.y - b.y)));
        return o == 0 ? 0 : o > 0 ? 1 : -1;
    }

    public static function pow(value:Float, exp:Float):Float
    {
        return HaxeMath.pow(value, exp);
    }

    public static function round(value:Float):Int
    {
        return HaxeMath.round(value);
    }

    public static function mapToRange(value:Float, min1:Float, max1:Float,
        min2:Float, max2:Float):Float
    {
        return min2 + ((value - min1) / (max1 - min1)) * (max2 - min2);
    }

    public static function sign(value:Float):Int
    {
        if (value >= 0)
        {
            return 1;
        }
        return -1;
    }

    public static function sin(angle:Float):Float
    {
        return HaxeMath.sin(angle);
    }

    public static function sqrt(value:Float):Float
    {
        return HaxeMath.sqrt(value);
    }

    public static function square(value:Float):Float
    {
        return value * value;
    }

    public static function tan(angle:Float):Float
    {
        return HaxeMath.tan(angle);
    }
}
