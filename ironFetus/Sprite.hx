package ironFetus;

import ironFetus.Graphic;


class Sprite
{
    public var active:Bool;
    public var graphics:Array<Graphic>;

    public function new()
    {
        active = true;
        graphics = [];
    }

    public function create():Void
    {
        active = true;
    }

    public function destroy():Void
    {
        active = false;
    }

    public function preUpdate(delta:Float):Void
    {
    }

    public function update(delta:Float):Void
    {
    }

    public function postUpdate(delta:Float):Void
    {
    }

    public function dispose():Void
    {
        active = false;
        graphics = [];
    }
}
