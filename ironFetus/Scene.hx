package ironFetus;

import ironFetus.Camera;
import ironFetus.Fe.gl;
import ironFetus.Sprite;
import ironFetus.rendering.Renderer;


class Scene
{
    public var renderers:Array<Renderer>;
    public var sprites:Array<Sprite>;
    public var cameras:Array<Camera>;

    public var backgroundColor:Int;

    private var initialized:Bool;

    public function new()
    {
        renderers = [];
        sprites = [];
        cameras = [];

        initialized = false;

        backgroundColor = 0xFF000000;

        gl.blendFunc(gl.SRC_ALPHA, gl.ONE_MINUS_SRC_ALPHA);
        gl.enable(gl.BLEND);
    }

    public function init():Void
    {
        throw "Method not implemented";
    }

    public function restart():Void
    {
        deactivate();

        renderers = [];
        sprites = [];
        cameras = [];

        backgroundColor = 0xFF000000;

        activate();
    }

    public function activate():Void
    {
        initialized = false;

        init();

        initialized = true;
    }

    public function deactivate():Void
    {
        initialized = false;

        for (renderer in renderers)
        {
            renderer.dispose();
        }

        for (sprite in sprites)
        {
            sprite.dispose();
        }
    }

    public function preUpdate(delta:Float):Void
    {
        if (initialized)
        {
            for (sprite in sprites)
            {
                if (sprite.active)
                {
                    sprite.preUpdate(delta);
                }
            }
        }
    }

    public function update(delta:Float):Void
    {
        if (initialized)
        {
            for (sprite in sprites)
            {
                if (sprite.active)
                {
                    sprite.update(delta);
                }
            }
        }
    }

    public function postUpdate(delta:Float):Void
    {
        if (initialized)
        {
            for (sprite in sprites)
            {
                if (sprite.active)
                {
                    sprite.postUpdate(delta);
                }
            }
        }
    }

    public function render():Void
    {
        gl.clearColor(
            Color.r(backgroundColor) / 0xFF,
            Color.g(backgroundColor) / 0xFF,
            Color.b(backgroundColor) / 0xFF,
            Color.a(backgroundColor) / 0xFF);
        gl.clear(gl.COLOR_BUFFER_BIT);

        if (initialized)
        {
            for (renderer in renderers)
            {
                renderer.update();
            }

            for (camera in cameras)
            {
                for (renderer in renderers)
                {
                    renderer.render(camera);
                }
            }
        }
    }
}
