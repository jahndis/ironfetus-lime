package ironFetus;

import haxe.Timer in HaxeTimer;


class Timer
{
    public var interval(default, null):Int;
    public var onComplete(default, null):Void->Void;
    public var repeatCount(default, null):Int;
    public var currentRepeatCount:Int;
    public var running(get, null):Bool;

    private var timer:HaxeTimer;
    private var delayTimer:HaxeTimer;

    public function new(i:Int, cb:Void->Void)
    {
        interval = i;
        onComplete = cb;
        repeatCount = 0;
        currentRepeatCount = 0;
    }

    public function start(?delay:Int=0, ?newRepeatCount:Int=0):Void
    {
        stop();

        repeatCount = newRepeatCount;
        currentRepeatCount = 0;

        delayTimer = HaxeTimer.delay(function () {
            onTimerComplete();

            if (repeatCount > 0)
            {
                timer = new HaxeTimer(interval);
                timer.run = onTimerComplete;
            }
        }, delay);
    }

    public function stop():Void
    {
        if (timer != null)
        {
            timer.stop();
            timer = null;
        }

        if (delayTimer != null)
        {
            delayTimer.stop();
            delayTimer = null;
        }
    }

    private function onTimerComplete():Void
    {
        currentRepeatCount += 1;

        if (currentRepeatCount >= repeatCount && repeatCount >= 0)
        {
            stop();
        }

        onComplete();
    }

    private function get_running():Bool
    {
        return timer != null;
    }
}
