package ironFetus;

import Math in HaxeMath;

import ironFetus.Math;

class Random
{
    private static var MULTIPLIER:Float = 48271.0;
    private static var MODULUS:Int = Math.MAX_INT;

    public var initialSeed(default, null):Int;
    public var currentSeed(get, null):Int;

    private var internalSeed:Float;

    public function new(?seed:Int)
    {
        if (seed == null)
        {
            seed = Std.int(HaxeMath.random() * MODULUS);
        }

        if (seed <= 0)
        {
            seed += MODULUS - 1;
        }

        initialSeed = seed;
        internalSeed = initialSeed;
    }

    public function float(?min:Float=0, ?max:Float=1,
        ?exclude:Array<Float>):Float
    {
        if (min == 0 && max == 1 && exclude == null)
        {
            return generate() / MODULUS;
        }
        else if (min == max)
        {
            return min;
        }
        else
        {
            if (min > max)
            {
                min = min + max;
                max = min - max;
                min = min - max;
            }

            var value:Float = min + (generate() / MODULUS) * (max - min);

            if (exclude != null)
            {
                while (exclude.indexOf(value) >= 0)
                {
                    value = min + (generate() / MODULUS) * (max - min);
                }
            }

            return value;
        }
    }

    public function int(?min:Int=0, ?max:Int=0x7FFFFFFF,
        ?exclude:Array<Int>):Int
    {
        if (min == 0 && max == Math.MAX_INT && exclude == null)
        {
            return Std.int(generate());
        }
        else if (min == max)
        {
            return min;
        }
        else
        {
            if (min > max)
            {
                min = min + max;
                max = min - max;
                min = min - max;
            }

            var value:Int = Std.int(min + (generate() / MODULUS) *
                (max - min + 1));

            if (exclude != null)
            {
                while (exclude.indexOf(value) >= 0)
                {
                    value = Std.int(min + (generate() / MODULUS) *
                        (max - min + 1));
                }
            }

            return value;
        }
    }

    @:generic
    public function shuffle<T>(array:Array<T>):Void
    {
        var tmp:T;
        var j:Int;
        for (i in 0...array.length - 1)
        {
            j = int(i, array.length - 1);
            tmp = array[i];
            array[i] = array[j];
            array[j] = tmp;
        }
    }

    private function get_currentSeed():Int
    {
        return Std.int(internalSeed);
    }

    private function generate():Float
    {
        internalSeed = (internalSeed * MULTIPLIER) % MODULUS;
        return internalSeed;
    }
}
